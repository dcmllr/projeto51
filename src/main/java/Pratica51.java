/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;

public class Pratica51 {

    public static void main(String[] args) {
        try{
            Matriz orig = new Matriz(3, 2);
            double[][] m = orig.getMatriz();
            m[0][0] = 0.0;
            m[0][1] = 0.1;
            m[1][0] = 1.0;
            m[1][1] = 1.1;
            m[2][0] = 2.0;
            m[2][1] = 2.1;
            Matriz transp = orig.getTransposta();
            Matriz soma = orig.soma(orig);
            Matriz prod = orig.prod(transp);
            System.out.println("Matriz original: " + orig);
            System.out.println("Matriz transposta: " + transp);
            System.out.println("Matriz soma: " + soma);
            System.out.println("Matriz produto: " + prod);
            
            Matriz erro = new Matriz(-1, 0);
        }catch(MatrizInvalidaException b){
            System.out.println(b.getMessage());
        }
    }
}